import java.net.URL
import java.net.URLClassLoader
import java.lang.Thread

sysClassLoader = URLClassLoader::getSystemClassLoader
urls = sysClassLoader.getURLs

urls.each {|u| puts u }

threadLoader = Thread::currentThread.getContextClassLoader
urls = threadLoader.getURLs

urls.each {|u| puts u }

