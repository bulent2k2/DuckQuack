import javax.script.ScriptEngine
import javax.script.ScriptEngineManager

mgr = ScriptEngineManager.new
factories = mgr.getEngineFactories() 

factories.each { |f|
   puts f.getEngineName()
   puts f.getEngineVersion()
   puts f.getLanguageName()
   puts f.getLanguageVersion()
}