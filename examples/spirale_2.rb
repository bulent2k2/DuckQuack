
pulisci

turtle.pen_size = 2
turtle.move_to(206, 0)

160.times.each {|n|
  color = Color.rgb(n, 255, 255 - n) 
  turtle.pen_color = color
  turtle.forward(10 + n/10)
  turtle.right(-90 + n)
  turtle.dot(size: 6)
  turtle.quad(size: 16, filled: false)
}