
reset

t = Turtle.new(canvas)

t.move_to(0,-100)

rules = {
  'G': "|[+G]|[-G]+G"
}

axiom = "G"

constants = ['X']

actions = {
  '_': ->(g, i) { t.forward(20) * (20 ** g) },
  'F': ->(g, i) { t.forward(20) },
  'f': ->(g, i) { t.forward(20) },
  'G': ->(g, i) {
    t.pen_up
    t.forward(50)
    t.pen_down
  },
  '[': ->(g, i) { t.save_position },
  ']': ->(g, i) { t.restore_position },
  '+': ->(g, i) { t.left(25) },
  '-': ->(g, i) { t.right(25) }
}

lsystem = LSystem.new(axiom, rules, constants, 4)

puts lsystem.system

lsystem.execute(actions)
