
reset

t = Turtle.new(canvas)

t.move_to(0,-100)

rules = {
  'G': "|[+G]|[-G]+G"
}

axiom = "G"

constants = ['X']

actions = {
  '_': ->(g, i) { t.forward(20) * (20 ** g) },
  'F': ->(g, i) { t.forward(20) },
  'f': ->(g, i) { t.forward(20) },
  'G': ->(g, i) {
    t.pen_up
    t.forward(50)
    t.pen_down
  },
  '[': ->(g, i) { t.save_position },
  ']': ->(g, i) { t.restore_position },
  '+': ->(g, i) { t.left(25) },
  '-': ->(g, i) { t.right(25) }
}

class LSystem

  attr :system

  MAX_GENERATIONS = 3

  def initialize(axiom, rules, constants, generations)
    @axiom, @rules, @constants, @generations = axiom, rules, constants, generations
    @action_calls = {}
    @system = evolve_with_rules
  end

  def is_digit(c)
    c == '0' ? true : !c.to_s.to_i.zero?
  end
  private :is_digit

  def evolve_with_rules
    apply_rules = lambda { |s, gen|
      @rules.reduce(s) { |sys, r|
        @constants.include?(r[0]) ? sys : sys.gsub(r[0].to_s, r[1])
      }.gsub('|', gen.to_s)
    }
    logger.info("L-System max generations is #{MAX_GENERATIONS}") if MAX_GENERATIONS < @generations
    (1...@generations).to_a[0, MAX_GENERATIONS].reduce(@axiom) { |sys, i| apply_rules.call(sys, i) } 
  end
  private :evolve_with_rules

  def execute_default_action(actions, g, i)
    actions[:'_'].call(g, i) unless actions[:'_'].nil?
  end
  private :execute_default_action

  def update_grow_iters(action, generation)
    if is_digit(action)
      if @action_calls.include?(action)
        @action_calls[action] += 1
        [generation, @action_calls[action]]
      else
        @action_calls[action] = 0
        [0, 0]
      end
    else
      [0, 0]
    end
  end

  def execute(actions)
    generation = '0'
    chars = @system.split(//)

    default_generation_action = ->(i) {
      if(generation.size != 1)
        execute_default_action(actions, generation.to_i, i)
        generation = '0'
      end
    }

    chars.each_index { |i|
      action = chars[i].to_sym
      generation << action.to_s if is_digit(action)
      (g, i) = update_grow_iters(action, generation.to_i)
      if !is_digit(action) && actions.include?(action)
        actions[action].call(g, i)
      else
        default_generation_action.call(i)
      end
    }
  end
end

lsystem = LSystem.new(axiom, rules, constants, 5)

p lsystem.system

lsystem.execute(actions)
